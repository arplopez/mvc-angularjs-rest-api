﻿using Movie.Data;
using System.Collections.Generic;

namespace Movie.Repository
{
    public interface IMovieRepository : IRepository<Movie.Data.Movie>
    {
    }
}

﻿using Movie.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Movie.Repository
{
    public class MovieRepository : IMovieRepository
    {

        MovieContext _movieContext;

        public MovieRepository()
        {
            _movieContext = new MovieContext();

        }
        public IEnumerable<Movie.Data.Movie> List
        {
            get
            {
                return _movieContext.Movies.ToList();
            }

        }

        public void Save(Movie.Data.Movie entity)
        {
            if (entity.Id > 0)
                _movieContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            else
                _movieContext.Movies.Add(entity);

            _movieContext.SaveChanges();
        }

        public void Delete(Movie.Data.Movie entity)
        {
            _movieContext.Movies.Remove(entity);
            _movieContext.SaveChanges();
        }

        public Movie.Data.Movie FindById(int Id)
        {
            var result = (from r in _movieContext.Movies where r.Id == Id select r).FirstOrDefault();
            return result;
        }
    }
}
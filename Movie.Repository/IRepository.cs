﻿using Movie.Data;
using System.Collections.Generic;

namespace Movie.Repository
{
    public interface IRepository<T> where T : IEntity
    {

        IEnumerable<T> List { get; }
        void Save(T entity);
        void Delete(T entity);
        T FindById(int Id);
    }
}

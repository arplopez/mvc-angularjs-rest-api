﻿using Movie.Data;
using System.Collections.Generic;

namespace Movie.Repository
{
    public interface IRatingRepository : IRepository<Movie.Data.Rating>
    {
    }
}

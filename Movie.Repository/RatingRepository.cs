﻿using Movie.Data;
using System.Collections.Generic;
using System.Linq;

namespace Movie.Repository
{
    public class RatingRepository : IRatingRepository
    {

        MovieContext _movieContext;

        public RatingRepository()
        {
            _movieContext = new MovieContext();

        }
        public IEnumerable<Movie.Data.Rating> List
        {
            get
            {
                return _movieContext.Ratings.ToList();
            }
        }

        public void Save(Movie.Data.Rating entity)
        {
            if (entity.Id > 0)
                _movieContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            else 
                _movieContext.Ratings.Add(entity);

            _movieContext.SaveChanges();
        }

        public void Delete(Movie.Data.Rating entity)
        {
            _movieContext.Ratings.Remove(entity);
            _movieContext.SaveChanges();
        }

        public Movie.Data.Rating FindById(int Id)
        {
            var result = (from r in _movieContext.Ratings where r.Id == Id select r).FirstOrDefault();
            return result;
        }
    }
}
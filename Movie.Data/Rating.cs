﻿namespace Movie.Data
{
    public class Rating : IEntity
    {
        public short Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}

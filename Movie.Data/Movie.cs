﻿namespace Movie.Data
{
    public class Movie: IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int  Year { get; set; }
        public short RatingId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Movie.Web.Helpers
{
    public static class ModelConversion
    {
        public static List<Movie.Web.Models.Movie> ToModel(this IEnumerable<Movie.Data.Movie> movies, List<Movie.Web.Models.Rating> ratings)
        {
            var model = new List<Movie.Web.Models.Movie>();

            if (movies.Any())
                movies.ToList().ForEach(m => {
                    var rating = (!ratings.Any()) ? null : ratings.Where(r => r.Id == m.RatingId).FirstOrDefault();
                    model.Add(new Models.Movie
                    {
                        Id = m.Id,
                        Title = m.Title,
                        Year = m.Year,
                        Rating = (rating == null) ? null : new Models.Rating { Id = rating.Id, Code = rating.Code, Description = rating.Description }
                    });
                });

            return model;
        }

        public static Movie.Web.Models.Movie ToModel(this Movie.Data.Movie movie, List<Movie.Web.Models.Rating> ratings)
        {
            var rating = (!ratings.Any()) ? null : ratings.Where(r => r.Id == movie.RatingId).FirstOrDefault();

            var model = (movie == null) ? null : new Models.Movie
            {
                Id = movie.Id,
                Title = movie.Title,
                Year = movie.Year,
                Rating = (rating == null) ? null : new Models.Rating { Id = rating.Id, Code = rating.Code, Description = rating.Description }
            };

            return model;
        }

        public static Movie.Data.Movie ToEntity(this Movie.Web.Models.Movie movie)
        {
            var entity = (movie == null) ? null : new Data.Movie
            {
                Id = movie.Id,
                Title = movie.Title,
                Year = movie.Year,
                RatingId = movie.Rating.Id
            };

            return entity;
        }

        public static List<Movie.Web.Models.Rating> ToModel(this IEnumerable<Movie.Data.Rating> ratings)
        {
            var model = new List<Movie.Web.Models.Rating>();

            if (ratings.Any()) ratings.ToList().ForEach(m => {
                model.Add(new Models.Rating
                {
                    Id = m.Id,
                    Code = m.Code,
                    Description = m.Description
                });
            });

            return model;
        }

        public static Movie.Web.Models.Rating ToModel(this Movie.Data.Rating rating)
        {
            var model = (rating == null) ? null : new Models.Rating
            {
                Id = rating.Id,
                Code = rating.Code,
                Description = rating.Description
            };

            return model;
        }

        public static Movie.Data.Rating ToEntity(this Movie.Web.Models.Rating rating)
        {
            var entity = (rating == null) ? null : new Data.Rating
            {
                Id = rating.Id,
                Code = rating.Code,
                Description = rating.Description
            };

            return entity;
        }
    }
}
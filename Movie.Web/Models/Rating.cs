﻿namespace Movie.Web.Models
{
    public class Rating
    {
        public short Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
﻿namespace Movie.Web.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }        
        public Rating Rating { get; set; }
    }
}
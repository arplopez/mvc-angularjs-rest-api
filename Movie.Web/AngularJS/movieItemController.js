﻿app.controller("movieItemController", function ($scope,
    $filter,
    movieService,
    $window,
    $http,
    $log,
    $interval,
    $uibModalInstance,
    $uibModal,
    row,
    ratings)   
{
    $scope.Ratings = angular.copy(ratings);
    $scope.selectedRating = null;

    if (row === null) {
        $scope.title = "Add Movie";
        $scope.rowData = {
            Id: 0, Title: null, Year: null, Rating: { Id: 0, Code: null, Description: null } };
    }
    else {
        $scope.title = "Edit Movie";
        $scope.rowData = angular.copy(row);
        $scope.selectedRating = $scope.Ratings.find(r => r.Id === $scope.rowData.Rating.Id);
    }
    $scope.add = function () {
        if (confirm("Are you sure you want to save these changes?") === false) {
            return;
        }
        else {
            var saveData = {
                Id: $scope.rowData.Id,
                Title: $scope.rowData.Title,
                Year: $scope.rowData.Year,
                Rating: $scope.selectedRating
            };
            movieService.saveMovie(saveData).then(function () {
                console.log("Successfullly Added.");
                $scope.showSuccessMessage = true;
                setTimeout($scope.close(), 1000);
            }, function (error) {
                $window.alert('Error occurred while saving movie.');
            });
        }
    };
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});  
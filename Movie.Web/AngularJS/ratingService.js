﻿app.service('ratingService', function ($http) {
    this.getRatingss = function () {
        var req = $http.get('api/Rating/GetAll');
        return req;
    };

    this.getRatingById = function (id) {
        var req = $http.get('api/Rating/GetById?id=' + id);
        return req;
    };

    this.saveRating = function (saveData) {
        var req = $http.post('api/Rating/Save', JSON.stringify(saveData),
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        return req;
    };

    this.deleteRating = function (id) {
        var req = $http.post('api/Rating/Delete?id=' + id);
        return req;
    };
});  
﻿app.controller("movieController", function (
    $scope, $uibModal,
    movieService, ratingService, $window) {
    var vm = this;

    init();

    function init() {
        ratingService.getRatingss().then(function (result) {
            $scope.Ratings = result.data;
        }, function (error) { });
        movieService.getMovies().then(function (result) {
            $scope.Movies = result.data;
            $scope.gridOptions.data = $scope.Movies;
        }, function (error) {
            $window.alert('Oops! Something went wrong while fetching movie data.');
        });

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 10
        };

        $scope.gridOptions = {
            enableRowSelection: true,
            selectionRowHeaderWidth: 35,
            enableRowHeaderSelection: false,
            paginationPageSizes: [10, 20, 30, 40],
            paginationPageSize: 10,
            enableSorting: true,
            columnDefs: [
                { name: '', field: 'Id', enableColumnMenu: false, cellTemplate: '<button title="Edit" class="btn btn-xs btn-primary fa fa-edit" ng-click="grid.appScope.editMovie(row)">Edit </button>&nbsp;&nbsp;<button title="Delete" class="btn btn-xs btn-danger fa fa-delete" ng-click="grid.appScope.deleteMovie(row)">Delete </button>', width: 100, pinnedLeft: false, enableHiding: false, exporterSuppressExport: true, enableSorting: false, enableFiltering: false },
                { name: 'Title', field: 'Title', headerCellClass: 'tablesorter-header-inner', enableFiltering: true, enableCellEdit: true },
                { name: 'Year Released', field: 'Year', headerCellClass: 'tablesorter-header-inner', enableFiltering: true, enableCellEdit: true },
                { name: 'Rating', field: 'Rating.Code', headerCellClass: 'tablesorter-header-inner', enableFiltering: true, enableCellEdit: true }
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            enableFiltering: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
    }

    $scope.addMovie = function () {     
        var modalInstance = $uibModal.open({
            templateUrl: 'AngularJS/movieItem.html',
            controller: 'movieItemController',
            size: 'md',
            backdrop: 'static',
            keyboard: false, 
            resolve: {
                row: function () { return null; },
                ratings: function () { return $scope.Ratings; }
            } 
        }).closed.then(function () {
            $scope.RefreshGridData();
            $scope.showGrid = true;
        }, function () { }
        );
    };

    $scope.editMovie = function (row) {
        var modalInstance = $uibModal.open({            
            templateUrl: 'AngularJS/movieItem.html',
            controller: 'movieItemController', 
            size: 'md', 
            backdrop: 'static',
            keyboard: false, 
            resolve: {
                row: function () { return row.entity; },
                ratings: function () { return $scope.Ratings; }
            }
        }).closed.then(function () {
            $scope.RefreshGridData();
            $scope.showGrid = true;
        }, function () { }
        );
    };

    $scope.deleteMovie = function (row) {
        if (confirm("Are you sure you want to delete " + row.entity.Title + " movie?") === true) {
            movieService.deleteMovie(row.entity.Id).then(function () {
                $window.alert(row.entity.Title + " has been successfullly deleted.");
                $scope.RefreshGridData();
            }, function (error) {
                $window.alert('Error occurred while deleting the movie.');
            });
        }
    };

    $scope.RefreshGridData = function () {
        movieService.getMovies().then(function (result) {
            $scope.gridOptions.data = result.data;
        }, function (error) {
            $window.alert('Oops! Something went wrong while fetching movie data.');
        });
    };    
}); 
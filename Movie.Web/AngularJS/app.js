﻿var app = angular.module('movie', ['ngRoute',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.pagination',
    'ui.grid.autoResize',
    'ui.grid.expandable',
    'ui.grid.selection',
    'ui.grid.pinning',
    'ui.bootstrap'])
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                templateUrl: 'AngularJS/movie.html',
                controller: 'movieController'
            }).when('/', {
                templateUrl: 'AngularJS/movieItem.html',
                controller: 'movieItemController'
            });
    });
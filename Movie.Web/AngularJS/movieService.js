﻿app.service('movieService', function ($http) {
    this.getMovies = function () {
        var req = $http.get('api/Movie/GetAll');
        return req;
    };

    this.getMovieById = function (id) {
        var req = $http.get('api/Movie/GetById?id=' + id);
        return req;
    };

    this.saveMovie = function (saveData) {
        var req = $http.post('api/Movie/Save', JSON.stringify(saveData),
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        return req;
    };

    this.deleteMovie = function (id) {
        var req = $http.post('api/Movie/Delete?id=' + id);
        return req;
    };
});  
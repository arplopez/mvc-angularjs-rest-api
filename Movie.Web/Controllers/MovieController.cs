﻿using Movie.Repository;
using System.Web.Http;
using Movie.Web.Helpers;

namespace Movie.Web.Controllers
{
    [RoutePrefix("api/Movie")]
    public class MovieController : ApiController
    {
        IRepository<Movie.Data.Movie> _imovieRepository;
        IRepository<Movie.Data.Rating> _iratingRepository;

        public MovieController(IMovieRepository imovieRepository, IRatingRepository iratingRepository)
        {
            _imovieRepository = imovieRepository;
            _iratingRepository = iratingRepository;
        }

        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var items = _imovieRepository.List.ToModel(_iratingRepository.List.ToModel());
            
            if (items != null)
                return Ok(items);
            else
                return NotFound();
        }

        [Route("GetById")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            var item = _imovieRepository.FindById(id);
            if (item != null)
                return Ok(item.ToModel(_iratingRepository.List.ToModel()));
            else
                return NotFound();
        }


        [Route("Save")]
        [HttpPost]
        public IHttpActionResult Save(Movie.Web.Models.Movie Movie)
        {
            if (ModelState.IsValid)
            {
                _imovieRepository.Save(Movie.ToEntity());

                return Ok(Movie);
            }
            else
                return BadRequest(ModelState);
        }

        [Route("Delete")]
        [HttpPost]
        public IHttpActionResult Delete(int id)
        {
            var item = _imovieRepository.FindById(id);
            if (item != null)
            {
                _imovieRepository.Delete(item);
                return Ok(item);
            }
            else
                return NotFound();

        }
    }
}

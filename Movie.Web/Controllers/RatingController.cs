﻿using Movie.Repository;
using System.Web.Http;
using Movie.Web.Helpers;

namespace Movie.Web.Controllers
{
    [RoutePrefix("api/Rating")]
    public class RatingController : ApiController
    {
        IRepository<Movie.Data.Rating> _iratingRepository;

        public RatingController(IRatingRepository iratingRepository)
        {
            _iratingRepository = iratingRepository;
        }

        [Route("GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            var items = _iratingRepository.List.ToModel();
            if (items != null)
                return Ok(items);
            else
                return NotFound();
        }

        [Route("GetById")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            var item = _iratingRepository.FindById(id);
            if (item != null)
                return Ok(item.ToModel());
            else
                return NotFound();
        }

        [Route("Save")]
        [HttpPost]
        public IHttpActionResult Save(Movie.Web.Models.Rating rating)
        {
            if (ModelState.IsValid)
            {
                _iratingRepository.Save(rating.ToEntity());

                return Ok(rating);
            }
            else
                return BadRequest(ModelState);
        }
        
        [Route("Delete")]
        [HttpPost]
        public IHttpActionResult Delete(int id)
        {
            var item = _iratingRepository.FindById(id);
            if (item != null)
            {
                _iratingRepository.Delete(item);
                return Ok(item);
            }
            else
                return NotFound();

        }
    }
}
